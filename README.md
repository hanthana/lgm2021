# LGM2021

 LGM is an international meeting for discussion for free culture and art, graphics and design with Free and Open Source Software.

 We are bringing Libre Graphics Meeting to Sri Lanka in 2021! It's a wonderful event where code and design and arts meet on a one platform. We expect core developers of leading Free/Libre and Open Source design software such as Inkscape, GIMP, Krita and Blender to join the conference. Also planning on showcasing projects done with free and open source software. It is about intersection of creativity and technology and there will be something for everybody. Topics range from plain old visual design and animation to procedural generation, code-based parametric design, live visualisations, web publishing.

We are looking for projects from Sri Lanka and the region. If you are interested get in touch :)

Also, if you are using illegal cracked software for your design work, this is the time to simply stop and switch to ethical and free design tools. If you are interested in working on projects to showcase at the event, start now. 

If you are interested in volunteering as an organiser get in touch this page.